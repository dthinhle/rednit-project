import React, { Component } from 'react';
import { View, StatusBar,Text, TextInput, Animated,TouchableOpacity,StyleSheet } from 'react-native'
import md5 from 'md5'
import firebase from 'firebase'
import SignUp from './SignUp'





class FloatingLabelInput extends Component {
  state = {
    isFocused: false,
  };

  componentWillMount() {
    this._animatedIsFocused = new Animated.Value(this.props.value === '' ? 0 : 1);
  }

  handleFocus = () => this.setState({ isFocused: true });
  handleBlur = () => this.setState({ isFocused: false });

  componentDidUpdate() {
    Animated.timing(this._animatedIsFocused, {
      toValue: (this.state.isFocused || this.props.value !== '') ? 1 : 0,
      duration: 200,
    }).start();
  }

  render() {
    const { label, ...props } = this.props;
    const labelStyle = {
      position: 'absolute',
      left: 0,
      top: this._animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: [18, 0],
      }),
      fontSize: this._animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: [20, 14],
      }),
      color: this._animatedIsFocused.interpolate({
        inputRange: [0, 1],
        outputRange: ['#aaa', '#000'],
      }),
    };
    return (
      <View style={{ paddingTop: 18 }}>
        <Animated.Text style={labelStyle}>
          {label}
        </Animated.Text>
        <TextInput
          {...props}
          style={{ height: 26, fontSize: 20, color: '#000', borderBottomWidth: 1, borderBottomColor: '#555' }}
          onFocus={this.handleFocus}
          onBlur={this.handleBlur}
          blurOnSubmit
        />
      </View>
    );
  }
}



export default class App extends Component {
  state = {
    name:'',
    password: '',
    isLoadingComplete: false,

    
  };
  
  handleName = (text) => {
    this.setState({ name: text })
  }
  
  handlePassword = (text) => {
    this.setState({ password: text })
  }
 
  readUserData(name,pass) {
      pass = md5(pass)
      
    firebase.database().ref('Users/'+name).once('value', function (snapshot) {
        if(snapshot.val()!=null){
            if(snapshot.val().password == pass){
                alert("Đăng nhập ăn cc à 8======D")
            }
        }
        
    });
    

}

  render() {
    return (
      <View style={{ flex: 1, padding: 30}}>
        <StatusBar hidden />
        
        {/* go to sign up page */}
        <FloatingLabelInput
            label="Name"
            value={this.state.name}
            onChangeText={this.handleName}
          />
  
          <FloatingLabelInput
            secureTextEntry={true}  
            label="Password"
            value={this.state.password}
            onChangeText={this.handlePassword}
          />
  
          
          <TouchableOpacity
                 style = {styles.submitButton}
                 onPress = {
                    () => this.readUserData(this.state.name,this.state.password)
                 }>
                 <Text style = {styles.submitButtonText}> Login </Text>
          </TouchableOpacity>
            
        
            
        <TouchableOpacity
               style = {styles.submitButton}
               onPress = {
                () => this.props.navigation.navigate('SignUpScreen')
               }>
               <Text style = {styles.submitButtonText}> Sign Up </Text>
        </TouchableOpacity>
            
            
      </View>
    );
  }
}

const styles = StyleSheet.create({
  radio:{
    paddingTop:20,
    
  },
  submitButton: {
     backgroundColor: '#7a42f4',
     padding: 10,
     marginTop: 20,
     height: 40,
  },
  submitButtonText:{
     color: 'white'
  }
})