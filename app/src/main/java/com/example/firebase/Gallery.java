package com.example.firebase;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.nightonke.boommenu.BoomButtons.ButtonPlaceEnum;
import com.nightonke.boommenu.BoomButtons.HamButton;
import com.nightonke.boommenu.BoomButtons.OnBMClickListener;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.ButtonEnum;
import com.nightonke.boommenu.Piece.PiecePlaceEnum;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import net.alhazmy13.mediapicker.Image.ImagePicker;


public class Gallery extends AppCompatActivity {

    GridView gridView;
    DatabaseReference mDatabase;
    DatabaseReference mFirstTime;


    User user;

    ColorFilter colorFilter;

    FirebaseStorage storage;
    StorageReference storageReference;

    ImageAdapter imageAdapter ;
    Integer itemSeleted=1;

    ImageView preView;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);



        Intent intent = getIntent();
        user = (User) intent.getSerializableExtra("userInfo");



        // -------------------- Database's  ----------------------------------------------------

        mFirstTime = FirebaseDatabase.getInstance().getReference().child(user.name);
        mFirstTime.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(!dataSnapshot.hasChild("Images")){
                    mFirstTime.child("Images").child(Long.toString(1)).setValue(user.avatarUrl);
                    imageAdapter.notifyDataSetChanged();

                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        mDatabase = FirebaseDatabase.getInstance().getReference().child(user.name);



        storage = FirebaseStorage.getInstance();
        storageReference = storage.getReference();

        // --------------------- end of Database -----------------------------------------------

        // --------------------- gridView to show imagaes --------------------------------------

        imageAdapter = new ImageAdapter(this);
        gridView = findViewById(R.id.gridview);

        // ---------------------- end of gridView -------------------------------------------



        // -------------------- load images from database ---------------------------
        loadImages();

        // -------------------- end of load images from database --------------------

        //---------------- create menu -------------------
        BoomMenuButton bmb = findViewById(R.id.bmb2);
        bmb.setButtonEnum(ButtonEnum.Ham);
        bmb.setPiecePlaceEnum(PiecePlaceEnum.HAM_2);
        bmb.setButtonPlaceEnum(ButtonPlaceEnum.HAM_2);
        //---------------- end of create menu ------------

        // ---------------- button 1 : upload images -------------------------------
        HamButton.Builder builder1 = new HamButton.Builder()
                .listener(new OnBMClickListener() {
                    @Override
                    public void onBoomButtonClick(int index) {
                        chooser();
                    }
                })
                .normalImageDrawable(getResources().getDrawable(R.drawable.uploadimg))
                .normalTextRes(R.string.addImages)
                .imagePadding(new Rect(15,15,15,15));


        bmb.addBuilder(builder1);
        // ----------------- end of upload images ------------------------------------


        // ----------------- button2 delete images -------------------------------------------

        HamButton.Builder builder2 = new HamButton.Builder()
                .listener(new OnBMClickListener() {
                    @Override
                    public void onBoomButtonClick(int index) {
                        deleteImages(imageAdapter.deleteMap);
                    }
                })
                .normalImageDrawable(getResources().getDrawable(R.drawable.deleteimg))
                .normalTextRes(R.string.deleteImages)
                .imagePadding(new Rect(15,15,15,15));


        bmb.addBuilder(builder2);


        // ----------------- end of delete images ------------------------------------

    }

    public void loadImages(){
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange( DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild("Images")){
                    for(DataSnapshot ds : dataSnapshot.child("Images").getChildren()){
                                String Url = ds.getValue(String.class);
                                imageAdapter.list.add(Url);
                        }

                    }
                    imageAdapter.listToArray();
                    gridView.setAdapter(imageAdapter);


                    gridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                        @Override
                        public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                            ImageView imageView = (ImageView) view;
                            imageView.setColorFilter(Color.BLUE, PorterDuff.Mode.LIGHTEN);


                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                colorFilter = imageView.getColorFilter();

                                if(preView==null){
                                    preView = imageView;
                                    Toast.makeText(Gallery.this,"Đã chọn "+itemSeleted+" bức ảnh",Toast.LENGTH_SHORT).show();
                                    imageAdapter.deleteMap.put(position,imageAdapter.getItem(position).toString());
                                }
                                else if(preView.equals(imageView)==false){
                                    itemSeleted= itemSeleted+1;
                                    Toast.makeText(Gallery.this,"Đã chọn "+itemSeleted+" bức ảnh",Toast.LENGTH_SHORT).show();
                                    imageAdapter.deleteMap.put(position,imageAdapter.getItem(position).toString());
                                    preView = imageView;
                                }
                                else{

                                    Toast.makeText(Gallery.this,"Đã chọn "+itemSeleted+" bức ảnh",Toast.LENGTH_SHORT).show();
                                    imageAdapter.deleteMap.put(position,imageAdapter.getItem(position).toString());
                                }

                            }

                            return true;
                        }
                    });

                    gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            ImageView imageView = (ImageView) view;
                            if(colorFilter!=null){
                                try{
                                    if(imageView.getColorFilter().equals(colorFilter)){
                                        imageView.setColorFilter(0);
                                        if(itemSeleted>1){
                                            itemSeleted = itemSeleted - 1;
                                            Toast.makeText(Gallery.this,"Đã chọn "+itemSeleted+" bức ảnh",Toast.LENGTH_SHORT).show();
                                            imageAdapter.deleteMap.remove(position);

                                        }
                                        else{
                                            Toast.makeText(Gallery.this,"Huỷ chọn ảnh",Toast.LENGTH_SHORT).show();
                                            imageAdapter.deleteMap.remove(position);
                                            itemSeleted = 0;


                                        }

                                    }
                                }catch (Exception e){

                                }

                            }


                            else{

                                Intent intent = new Intent(getApplicationContext(),FullScreen.class);
                                intent.putExtra("images",imageAdapter.images);
                                intent.putExtra("id",position);
                                startActivity(intent);
                            }

                        }
                    });

                    // ----------------------- gridView's multiple select ---------------------



                    // ----------------------- end of multiple select -------------------------
                }

            @Override
            public void onCancelled( DatabaseError databaseError) {

            }
        });
    }

    public void chooser(){
        new ImagePicker.Builder(Gallery.this)
                .mode(ImagePicker.Mode.CAMERA_AND_GALLERY)
                .compressLevel(ImagePicker.ComperesLevel.MEDIUM)
                .directory(ImagePicker.Directory.DEFAULT)
                .extension(ImagePicker.Extension.PNG)
                .scale(300, 300)
                .allowMultipleImages(true)
                .enableDebuggingMode(true)
                .allowOnlineImages(true)
                .build();

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ImagePicker.IMAGE_PICKER_REQUEST_CODE && resultCode == RESULT_OK) {
            List<String> mPaths = data.getStringArrayListExtra(ImagePicker.EXTRA_IMAGE_PATH);
            for(int i =0;i<mPaths.size();i++){
                Uri filepath = Uri.fromFile(new File(mPaths.get(0)));
                uploadImage(filepath,i);
            }

        }
    }

    private void uploadImage(final Uri filePath,final int i){

        if(filePath != null)
        {
            final ProgressDialog progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("Uploading...");
            progressDialog.show();


            mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                            final StorageReference ref = storageReference.child(user.name +"/"+"images/"+ dataSnapshot.getKey()+"/"+ UUID.randomUUID().toString());
                            ref.putFile(filePath)
                                    .addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                        @Override
                                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                            progressDialog.dismiss();
                                            ref.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                                                @Override
                                                public void onSuccess(final Uri uri) {
                                                    long count = dataSnapshot.child("Images").getChildrenCount();

                                                    count = count+i+1;
                                                    mDatabase.child("Images").child(Long.toString(count)).setValue(uri.toString());
                                                    imageAdapter.updateImageList(uri.toString());
                                                    imageAdapter.notifyDataSetChanged();

                                                }
                                            });
                                            Toast.makeText(Gallery.this, "Uploaded", Toast.LENGTH_SHORT).show();

                                        }
                                    })
                                    .addOnFailureListener(new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            progressDialog.dismiss();
                                            Toast.makeText(Gallery.this, "Failed "+e.getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    })
                                    .addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                                        @Override
                                        public void onProgress(UploadTask.TaskSnapshot taskSnapshot) {
                                            double progress = (100.0*taskSnapshot.getBytesTransferred()/taskSnapshot
                                                    .getTotalByteCount());
                                            progressDialog.setMessage("Uploaded "+(int)progress+"%");
                                        }
                                    });




                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });


        }
    }


    private void deleteImages(HashMap<Integer,String> deleteMap){

        for(final HashMap.Entry<Integer,String> entry : deleteMap.entrySet()){

            // delete from storage
            StorageReference photoRef = storage.getReferenceFromUrl(entry.getValue());
            photoRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    imageAdapter.removeAt(entry.getKey());
                    imageAdapter.notifyDataSetChanged();
                }
            });

            // delete child in realtime DB

            mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    for(DataSnapshot ds : dataSnapshot.child("Images").getChildren()){
                                if(ds.getValue().toString().equals(entry.getValue())){
                                    FirebaseDatabase.getInstance().getReference()
                                            .child(user.name).child("Images")
                                            .child(ds.getKey()).setValue(null);
                                }




                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
        }

        itemSeleted=0;
    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        finish();
    }
}
