package com.example.firebase;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.nightonke.boommenu.BoomButtons.ButtonPlaceEnum;
import com.nightonke.boommenu.BoomButtons.HamButton;
import com.nightonke.boommenu.BoomButtons.OnBMClickListener;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.ButtonEnum;
import com.nightonke.boommenu.Piece.PiecePlaceEnum;
import com.nightonke.boommenu.Util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import static com.example.firebase.MainActivity.KEY_PASS;
import static com.example.firebase.MainActivity.KEY_REMEMBER;
import static com.example.firebase.MainActivity.KEY_USERNAME;
import static com.example.firebase.MainActivity.PREF_NAME;

public class MainMenu extends AppCompatActivity {

    User user;
    DatabaseReference mDatabase;
    ImageView imageView;
    TextView name;
    SharedPreferences.Editor editor;
    SharedPreferences sharedPreferences;
    TextView bio;
    int count=0;
    int sec=1;
    Date time1;
    Date time2;

    Spinner spinner;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title bar
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);



        setContentView(R.layout.activity_main_menu);
        mDatabase = FirebaseDatabase.getInstance().getReference();

        imageView = findViewById(R.id.userAvatar);
        name = findViewById(R.id.nameLabel);
        spinner = findViewById(R.id.genderPicker);

        try{
            sharedPreferences = getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
            editor = sharedPreferences.edit();
        }catch (Exception e){

        }



        imageView.setClipToOutline(true);
        bio = findViewById(R.id.bio);



        //---------------- getting userInfo --------------
        Intent intent = getIntent();
        user = (User) intent.getSerializableExtra("userInfo") ;
        //---------------- end of getting userInfo

        //---------------- create menu -------------------
        BoomMenuButton bmb = findViewById(R.id.bmb);
        bmb.setButtonEnum(ButtonEnum.Ham);
        bmb.setPiecePlaceEnum(PiecePlaceEnum.HAM_5);
        bmb.setButtonPlaceEnum(ButtonPlaceEnum.HAM_5);
        //---------------- end of create menu ------------

        //-------------------- below codes are for adding and styling buttons one by one --------------
        HamButton.Builder builder1 = new HamButton.Builder()
              .listener(new OnBMClickListener() {
                  @Override
                  public void onBoomButtonClick(int index) {
                        goToInfoPage(user);
                  }
              })
                .normalImageDrawable(getResources().getDrawable(R.drawable.user))
                .normalTextRes(R.string.userInfo)
                .imagePadding(new Rect(15,15,15,15));





        HamButton.Builder builder2 = new HamButton.Builder()
                .listener(new OnBMClickListener() {
                    @Override
                    public void onBoomButtonClick(int index) {
                        sendFeedback();
                    }
                })
                .normalImageDrawable(getResources().getDrawable(R.drawable.bug))
                .normalTextRes(R.string.feedback)
                .imagePadding(new Rect(15,15,15,15));


        HamButton.Builder builder3 = new HamButton.Builder()
                .listener(new OnBMClickListener() {
                    @Override
                    public void onBoomButtonClick(int index) {
                        BlockList();
                    }
                })
                .normalImageDrawable(getResources().getDrawable(R.drawable.blocklist))
                .normalTextRes(R.string.blocklist)
                .imagePadding(new Rect(15,15,15,15));


        HamButton.Builder builder4 = new HamButton.Builder()
                .listener(new OnBMClickListener() {
                    @Override
                    public void onBoomButtonClick(int index) {
                       gotochatHistory();
                    }
                })
                .normalImageDrawable(getResources().getDrawable(R.drawable.chathistory))
                .normalTextRes(R.string.chathistory)
                .imagePadding(new Rect(15,15,15,15));



        HamButton.Builder builder5 = new HamButton.Builder()
                .listener(new OnBMClickListener() {
                    @Override
                    public void onBoomButtonClick(int index) {
                        Gallery();
                    }
                })
                .normalImageDrawable(getResources().getDrawable(R.drawable.album))
                .normalTextRes(R.string.album)
                .imagePadding(new Rect(15,15,15,15));

        bmb.addBuilder(builder1);
        bmb.addBuilder(builder4);
        bmb.addBuilder(builder5);
        bmb.addBuilder(builder2);
        bmb.addBuilder(builder3);




        //-------------------- end of adding and styling buttons ---------------------------------

        // ------------------- display user for the first time -----------------------------------

        displayUser("Cả hai");

        // ------------------- end of display user -----------------------------------------------

        // ----------------- sort gender ---------------------------------------------

        GenderPicker();

        // ----------------- end of sort gender -------------------------------------



    }



    public void GenderPicker(){
        List<String> list = new ArrayList<String>();
        list.add("Cả hai");
        list.add("Nam");
        list.add("Nữ");

        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
        spinner.setPrompt(getResources().getString(R.string.sort));
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                displayUser(spinner.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void Gallery(){
        Intent intent = new Intent(this, Gallery.class);
        intent.putExtra("userInfo",user);
        startActivity(intent);
    }

    public void BlockList(){
        Intent intent = new Intent(this, BlockList.class);
        intent.putExtra("userInfo",user);
        startActivity(intent);
    }

    public void goToInfoPage(final User user){
        final Intent intent = new Intent(this, UserInformation.class);

        intent.putExtra("userInfo",user);
        this.finish();
        startActivity(intent);

    }

    public void sendFeedback(){
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:nngochoangnguyen@gmail.com"));
        intent.putExtra(Intent.EXTRA_EMAIL  , new String[] { "" });
        intent.putExtra(Intent.EXTRA_SUBJECT, "App's feedback");

        startActivity(Intent.createChooser(intent, "Feed back"));

    }



   public void gotochatHistory(){
       //TODO show chatted list .
       Intent intent = new Intent(MainMenu.this,ChatHistory.class);
       intent.putExtra("userInfo",user);
       startActivity(intent);
   }

    private String getAge(String dobString){

        Date date = null;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        try {
            date = sdf.parse(dobString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(date == null) return "";

        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.setTime(date);

        int year = dob.get(Calendar.YEAR);
        int month = dob.get(Calendar.MONTH);
        int day = dob.get(Calendar.DAY_OF_MONTH);

        dob.set(year, month+1, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)){
            age--;
        }



        return Integer.toString(age);
    }



    public void displayUser(final String gender){
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN_MR1)
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                final List<User> userList = new ArrayList<User>();
                List<String> blockList = new ArrayList<>();

                for(DataSnapshot dss : dataSnapshot.child(user.name).child("blocklist").getChildren()){
                    blockList.add(dss.getKey());
                }

                for(DataSnapshot ds : dataSnapshot.getChildren()){
                    if(!gender.equals("Cả hai")){
                        if(ds.hasChild("hide")){
                            if(ds.child("hide").hasChild("gender")){
                                if(!ds.getKey().equals(user.name) && !blockList.contains(ds.getKey())
                                        &&ds.child("gender").getValue().toString().equals(gender)
                                        &&!ds.child("hide").child("gender").getValue().toString().equals("true")){

                                    User user = ds.getValue(User.class);
                                    userList.add(user);
                                }
                            }
                        }
                        else{
                            if(!ds.getKey().equals(user.name) && !blockList.contains(ds.getKey())
                                    &&ds.child("gender").getValue().toString().equals(gender)){

                                User user = ds.getValue(User.class);
                                userList.add(user);
                            }
                        }

                    }
                    else{
                        if(!ds.getKey().equals(user.name) && !blockList.contains(ds.getKey())){
                            User user = ds.getValue(User.class);
                            userList.add(user);
                        }
                    }


                }

                Random random = new Random();


               try{
                   final User displayThis = userList.get(random.nextInt(userList.size()));
                   Glide.with(MainMenu.this).load(displayThis.avatarUrl).into(imageView);

                   String date = displayThis.date;
                   String nickname = displayThis.nickname;
                   name.setText(nickname+"\n"+getAge(date)+" Tuổi");
                   name.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);

                    if(dataSnapshot.child(displayThis.name).hasChild("bio")){
                        String bioS = dataSnapshot.child(displayThis.name).child("bio").getValue().toString();
                        bio.setText(bioS);
                        bio.setMovementMethod(new ScrollingMovementMethod());
                    }
                    else {
                        String defaultBio ="Chào bạn,mình là "+displayThis.nickname+" rất vui được làm quen!";
                        bio.setMaxLines(3);
                        bio.setText(defaultBio);
                        bio.setMovementMethod(new ScrollingMovementMethod());

                    }



                   imageView.setOnTouchListener(new OnSwipeTouchListener(MainMenu.this){
                       @Override
                       public void onClick() {
                           super.onClick();
                           ChatWithThisUser(displayThis);
                       }

                       @Override
                       public void onDoubleClick() {
                           super.onDoubleClick();
                           // your on onDoubleClick here
                       }

                       @Override
                       public void onLongClick() {
                           super.onLongClick();
                           AlertDialog.Builder adb = new AlertDialog.Builder(MainMenu.this);


                           adb.setMessage("Bạn có muốn block user này không ?");
                           adb.setTitle("Block user này");
                           adb.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                               @Override
                               public void onClick(DialogInterface dialog, int which) {
                                   mDatabase.child(user.name).child("blocklist").child(displayThis.name).setValue(displayThis.nickname);
                                   mDatabase.child(displayThis.name).child("blocklist").child(user.name).setValue(user.nickname);
                                   mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
                                       @Override
                                       public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                           if(dataSnapshot.child(user.name).child("chatbox").hasChild(displayThis.name)){
                                               mDatabase.child(user.name).child("chatbox").child(displayThis.name).child("blocked").setValue("true");

                                           }
                                           if(dataSnapshot.child(displayThis.name).child("chatbox").hasChild(user.name)){
                                               mDatabase.child(displayThis.name).child("chatbox").child(user.name).child("blocked").setValue("true");

                                           }
                                       }

                                       @Override
                                       public void onCancelled(@NonNull DatabaseError databaseError) {

                                       }
                                   });

                                   userList.remove(displayThis);
                                   displayUser(displayThis.gender);
                               }
                           });

                           adb.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                               @Override
                               public void onClick(DialogInterface dialog, int which) {

                               }
                           });

                           AlertDialog ad = adb.create();
                           ad.show();
                       }

                       @Override
                       public void onSwipeUp() {
                           super.onSwipeUp();
                           // your swipe up here
                           checkTheirInfo(displayThis);
                       }

                       @Override
                       public void onSwipeDown() {
                           super.onSwipeDown();
                           // your swipe down here.
                       }

                       @Override
                       public void onSwipeLeft() {
                           super.onSwipeLeft();
                           skipThisUser(spinner.getSelectedItem().toString());

                       }

                       @Override
                       public void onSwipeRight() {
                           super.onSwipeRight();
                           // your swipe right here.
                       }
                   });


               }catch (Exception e){

               }





            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
   }

    public void skipThisUser(String gender){
        displayUser(gender);
    }

    public void ChatWithThisUser(User withWhom){
        Intent intent = new Intent(MainMenu.this,Chat.class);
        intent.putExtra("withWhom",withWhom);
        intent.putExtra("userInfo",user);


        startActivity(intent);
    }



    public void checkTheirInfo(User who){
        final Intent intent = new Intent(MainMenu.this,TheirInformation.class);
        intent.putExtra("theirInfo",who);
        startActivity(intent);

    }


    @Override
    public void onBackPressed(){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm a");
        try {

            time1 = Calendar.getInstance().getTime();
            if(time2 == null){
                time2 = time1;
            }

                long diff = time1.getTime() - time2.getTime();
                Log.w("cac",time1.toString());
                Log.w("cac2",time2.toString());
                Log.w("cac",Long.toString(diff));
                if(diff<3000){
                    if(count<1){
                        Toast.makeText(this, "Nhấn back lần nữa để thoát", Toast.LENGTH_SHORT).show();
                    }


                    if(count >=1){
                        super.onBackPressed();
                        this.finish();
                        editor.putBoolean(KEY_REMEMBER, false);
                        editor.remove(KEY_PASS);//editor.putString(KEY_PASS,"");
                        editor.remove(KEY_USERNAME);//editor.putString(KEY_USERNAME, "");
                        editor.apply();
                    }
                    count = count+1;
                }
                else{
                    time1 = Calendar.getInstance().getTime();
                    time2 = null;
                    count = 0;
                }




        } catch (Exception e) {
            e.printStackTrace();
        }


    }
}
