package com.example.firebase;

public class Message {
    private User sender;
    private String message;
    private boolean isMine ;
    public Message(String message,User sender,Boolean isMine){
        this.message = message;
        this.sender = sender;
        this.isMine = isMine;
    }


    public String getMessage(){
        return this.message;
    }

    public User getSenderInfo(){
        return this.sender;
    }

    public Boolean getisMine(){
        return this.isMine;
    }
}
