package com.example.firebase;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import me.relex.circleindicator.CircleIndicator;

public class Instruction extends AppCompatActivity {

    private ViewPager viewPager;
    private CircleIndicator circleIndicator;
    private InstructionPager instructionPager;
    User user;
    String bio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instruction);

        Intent intent = getIntent();
        user = (User) intent.getSerializableExtra("userInfo");
        bio = intent.getStringExtra("bio");

        String[]images = {"https://firebasestorage.googleapis.com/v0/b/rednit-project.appspot.com/o/manual%2Fmanual_next.png?alt=media&token=51fe30c8-d847-412b-9287-21628e391b71",
           "https://firebasestorage.googleapis.com/v0/b/rednit-project.appspot.com/o/manual%2Fmanual_info.png?alt=media&token=70335829-8dfa-4cbb-b486-e739201c1102",
           "https://firebasestorage.googleapis.com/v0/b/rednit-project.appspot.com/o/manual%2Fmanual_chat.png?alt=media&token=87a303de-a5d5-4a6a-8a91-8b9ebb25d78b",
                "https://firebasestorage.googleapis.com/v0/b/rednit-project.appspot.com/o/manual%2Fmanual_block.png?alt=media&token=9bae43b3-7136-4ea3-8c3b-e0b17f9b8fee"
        };

        instructionPager = new InstructionPager(this,images,user,bio);
        viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(instructionPager);

        circleIndicator = findViewById(R.id.indicator_custom);
        circleIndicator.setViewPager(viewPager);

    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        Intent intent = new Intent(this,MainMenu.class);
        intent.putExtra("userInfo",user);
        intent.putExtra("bio",bio);
        finish();
        startActivity(intent);
    }
}
