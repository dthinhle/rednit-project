package com.example.firebase;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class BlockList extends AppCompatActivity {


    DatabaseReference mDatabase;
    DatabaseReference mDatabase2;

    User user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_block_list);
        setTitle("Danh sách bị chặn");

        Intent intent = getIntent();
        user =  (User) intent.getSerializableExtra("userInfo");
        loadBlockList();
    }


    public void loadBlockList(){
        mDatabase = FirebaseDatabase.getInstance().getReference().child(user.name);
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot ds : dataSnapshot.child("blocklist").getChildren()){
                        String chatName = ds.getKey().toString();
                        String chatNickname = ds.getValue().toString();
                        createButton(chatName,chatNickname);

                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }

    public void createButton(final String name,final String nickname){
        // Create a LinearLayout element
        final LinearLayout linearLayout = findViewById(R.id.LN_layout1);

        final LinearLayout hLinearLayout = new LinearLayout(this);
        hLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
        hLinearLayout.setPadding(0,10,0,10);

        TextView textView = new TextView(this);
        textView.setText(nickname);
        textView.setAllCaps(false);
        textView.setPadding(20,0,50,0);

        final Button button = new Button(this);
        // Add Buttons
        button.setText("Gỡ Block");
        button.setGravity(Gravity.LEFT|Gravity.CENTER);
        button.setAllCaps(false);

        hLinearLayout.addView(textView);
        hLinearLayout.addView(button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    unBlockFromMe(name);
                    unBlockFromThem(name);
                    linearLayout.removeView(hLinearLayout);
            }
        });
        linearLayout.addView(hLinearLayout);
    }


    public void unBlockFromMe(final String name){
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot ds : dataSnapshot.child("blocklist").getChildren()){
                    if(ds.getKey().toString().equals(name)){
                        mDatabase.child("blocklist").child(name).setValue(null);
                        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                if(dataSnapshot.child("chatbox").hasChild(name)){
                                    mDatabase.child("chatbox").child(name).child("blocked").setValue("false");

                                }
                            }

                            @Override
                            public void onCancelled(@NonNull DatabaseError databaseError) {

                            }
                        });
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    public void unBlockFromThem(final String name){
        mDatabase2 = FirebaseDatabase.getInstance().getReference().child(name);
        mDatabase2.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                for(DataSnapshot ds : dataSnapshot.child("blocklist").getChildren()){
                    if(ds.getKey().toString().equals(user.name)){
                        mDatabase2.child(user.name).setValue(null);
                        if(dataSnapshot.child("chatbox").hasChild(name)){
                            mDatabase2.child("chatbox").child(name).child("blocked").setValue("false");

                        }
                    }
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
    @Override
    public void onBackPressed(){
        super.onBackPressed();
        finish();
    }

}
