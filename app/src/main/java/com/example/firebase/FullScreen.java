package com.example.firebase;

import android.content.Intent;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.widget.ImageView;

import com.alexvasilkov.gestures.Settings;
import com.alexvasilkov.gestures.views.GestureImageView;
import com.bumptech.glide.Glide;

import me.relex.circleindicator.CircleIndicator;


public class FullScreen extends AppCompatActivity {


    private ViewPager viewPager;
    private CircleIndicator circleIndicator;
    private MyPager myPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen);

        Intent intent = getIntent();

        int position = intent.getExtras().getInt("id");
        String [] images = (String[]) intent.getSerializableExtra("images");
        ImageAdapter imageAdapter = new ImageAdapter(this);

        myPager = new MyPager(this,images);
        viewPager = findViewById(R.id.view_pager);
        viewPager.setAdapter(myPager);
        viewPager.setCurrentItem(position);

        circleIndicator = findViewById(R.id.indicator_custom);
        circleIndicator.setViewPager(viewPager);

    }

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        finish();
    }
}
