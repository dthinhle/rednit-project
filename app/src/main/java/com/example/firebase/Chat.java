package com.example.firebase;

import android.content.Intent;

import android.support.annotation.NonNull;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;


import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;



public class Chat extends AppCompatActivity {

    EditText editText;
    User user;
    MessageAdapter messageAdapter;
    Message messageObj;
    DatabaseReference chatRef;
    DatabaseReference recieverRef;
    DatabaseReference receiveMessageRef;
    long count ;
    ImageButton imageButton;
    String avatarUrl;
    User withWhom;




    int numberOfMessage = 10;

    ListView chatScreen;

    SwipeRefreshLayout swipeRefreshLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);


        editText = findViewById(R.id.editText);

        chatScreen = findViewById(R.id.messages_view);

        Intent intent = getIntent();
        user = (User)  intent.getSerializableExtra("userInfo"); // user info
        withWhom = (User)intent.getSerializableExtra("withWhom"); // chat with who ?



        imageButton = findViewById(R.id.sendButton);

        //------------------------------Database Ref use for chat ----------------------------------

        chatRef = FirebaseDatabase.getInstance().getReference().child(user.name).child("chatbox").child(withWhom.name);
        recieverRef = FirebaseDatabase.getInstance().getReference().child(withWhom.name);

        recieverRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                chatRef.child("avatarUrl")
                        .setValue(dataSnapshot.child("avatarUrl").getValue().toString());
                String theirNickname =  dataSnapshot.child("nickname").getValue().toString();
                setTitle("Chat with "+theirNickname);
                chatRef.child("nickname").setValue(theirNickname);
                chatRef.child("blocked").setValue("false");

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

        recieverRef.child("chatbox").child(user.name).child("avatarUrl").setValue(user.avatarUrl);
        recieverRef.child("chatbox").child(user.name).child("nickname").setValue(user.nickname);
        recieverRef.child("chatbox").child(user.name).child("blocked").setValue("false");




        // -------------------------------- end of Database Ref ------------------------------------

        // -------------------------------- set chat screen adapter --------------------------------

        messageAdapter = new MessageAdapter(Chat.this);
        chatScreen.setAdapter(messageAdapter);

        // -------------------------------- end of set chat screen adapter ------------------------

        // -------------------------- load 10 last message for the first time ------------------

        loadOldMessages();

        // -------------------------- end of load 10 last message ------------------------------

        // -------------------------- Live incomming chat message ------------------------------

        Timer timer = new Timer();
        //Set the schedule function
        timer.scheduleAtFixedRate(new TimerTask() {
                                      @Override
                                      public void run() {

                                          chatRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                              @Override
                                              public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                                  if(dataSnapshot.hasChild("new")){
                                                      Boolean check = dataSnapshot.child("new").getValue().equals("true");
                                                      if(check){
                                                          loadOldMessages();
                                                          chatRef.child("new")
                                                                  .setValue("false");

                                                      }
                                                  }
                                                  else{
                                                      chatRef.child("new").setValue("false");
                                                  }

                                              }

                                              @Override
                                              public void onCancelled(@NonNull DatabaseError databaseError) {

                                              }
                                          });

                                      }
                                  },
                0, 250);

        // ---------------------------------- end of live incomming chat message -------------------


        // ------------------------- swipe to load 10 more message ---------------------------------

        swipeRefreshLayout = findViewById(R.id.pullToRefresh);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                numberOfMessage = numberOfMessage +10;
                swipeRefreshLayout.setRefreshing(false);
                loadOldMessages();
            }
        });

        // -------------------------- end of swipe to load 10 more message ------------------------



    }


    // ----------------- send message to people ----------------------------------------------------

    public void sendMessage(View view) {
        String message = editText.getText().toString();
        if (message.length() > 0) {
            //TODO : send message to listview and firebase

            final Message messageObj = new Message(message,user,true);
            messageAdapter.add(messageObj);
            chatScreen.setAdapter(messageAdapter);
            chatScreen.setSelection(messageAdapter.getCount()-1);

            //------------------------ add message to firebase --------------------------
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy 'at' HH:mm:ss ");
            final String currentDateandTime = sdf.format(new Date());

            // ------------------- add user message to db---------------------------------
            chatRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    count = dataSnapshot.getChildrenCount()-3;

                    chatRef.child("new").setValue("false");
                    chatRef.child(Long.toString(count)).child("time").setValue(currentDateandTime);
                    chatRef.child(Long.toString(count)).child("isMine").setValue(messageObj.getisMine());
                    chatRef.child(Long.toString(count)).child("message").setValue(messageObj.getMessage());
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });


            // ------------------- end of add user message to db --------------------------

            // ------------------- add message to reciever db ----------------------------
            recieverRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {


                    recieverRef.child("chatbox").child(user.name).child(Long.toString(count))
                            .child("isMine").setValue(!messageObj.getisMine());
                    recieverRef.child("chatbox").child(user.name).child(Long.toString(count))
                            .child("time").setValue(currentDateandTime);

                    recieverRef.child("chatbox").child(user.name).child(Long.toString(count))
                            .child("message").setValue(messageObj.getMessage());

                    recieverRef.child("chatbox").child(user.name).child("new").setValue("true");


                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });




            // ------------------- end of add message to reciever db ---------------------

            //------------------------ end of add message to firebase --------------------


        }
        imageButton.setEnabled(false);

        Timer buttonTimer = new Timer();
        buttonTimer.schedule(new TimerTask() {

            @Override
            public void run() {
                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {
                        imageButton.setEnabled(true);
                    }
                });
            }
        }, 1000);


        editText.getText().clear();

    }

    // ----------------- end of send message to people ---------------------------------------------








    // ----------------- load message from db ------------------------------------------------------

    public void loadOldMessages() {
        messageAdapter.clearList();
        receiveMessageRef = FirebaseDatabase.getInstance().getReference()
                .child(user.name).child("chatbox").child(withWhom.name); // to prevent conflict with chatRef

        //TODO : listen and recieve message then display new message in listView
        receiveMessageRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull final DataSnapshot dataSnapshot) {
                long childCount = dataSnapshot.getChildrenCount()-2;
                long start = childCount - numberOfMessage+1;

                if(start < 0){
                    start = 1;
                }
                try{
                    avatarUrl = dataSnapshot.child("avatarUrl").getValue().toString();

                    if(childCount >=2){
                        for(long i = start;i<=childCount;i++){
                            if(dataSnapshot.hasChild(Long.toString(i))){
                                if (dataSnapshot.child(Long.toString(i)).child("isMine").getValue().toString().equals("false")) {
                                    User sender = withWhom;
                                    try{
                                        String message = dataSnapshot.child(Long.toString(i)).child("message")
                                                .getValue().toString();

                                        messageObj = new Message(message, sender, false);
                                        messageAdapter.add(messageObj);

                                        chatScreen.setSelection(messageAdapter.getCount() - 1);


                                    }
                                    catch (Exception e){

                                    }

                                }
                                else{

                                    User sender = withWhom;

                                    try{
                                        String message =dataSnapshot.child(Long.toString(i)).child("message")
                                                .getValue().toString();
                                        messageObj = new Message(message, sender, true);
                                        messageAdapter.add(messageObj);
                                        chatScreen.setSelection(messageAdapter.getCount() - 1);

                                    }
                                    catch (Exception e){

                                    }
                                }
                            }


                        }
                    }
                }
                catch (Exception e){

                }


            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });



    }

    // ----------------- end of load message from db -----------------------------------------------

    @Override
    public void onBackPressed(){
        super.onBackPressed();
        finish();
    }

}

