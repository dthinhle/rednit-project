package com.example.firebase;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.alexvasilkov.gestures.Settings;
import com.alexvasilkov.gestures.views.GestureImageView;
import com.bumptech.glide.Glide;

public class InstructionPager extends PagerAdapter {
    private Context context;
    User user;
    String bio;

    String [] imagesLinks;


    public InstructionPager(Context context, String[]imagesLinks,User user,String bio){
        this.context = context;
        this.imagesLinks = imagesLinks;
        this.user = user;
        this.bio = bio;
    }

    /*
    This callback is responsible for creating a page. We inflate the layout and set the drawable
    to the ImageView based on the position. In the end we add the inflated layout to the parent
    container .This method returns an object key to identify the page view, but in this example page view
    itself acts as the object key
    */
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public Object instantiateItem(final ViewGroup container, int position) {
        View view = LayoutInflater.from(context).inflate(R.layout.instruction_pager, null);
        final GestureImageView imageView = view.findViewById(R.id.imageViewFull);
        imageView.getController().getSettings()
                .setMaxZoom(2f)
                .setDoubleTapZoom(-1f) // Falls back to max zoom level
                .setPanEnabled(true)
                .setZoomEnabled(true)
                .setDoubleTapEnabled(true)
                .setRotationEnabled(false)
                .setRestrictRotation(true)
                .setOverscrollDistance(0f, 0f)
                .setOverzoomFactor(2f)
                .setFillViewport(false)
                .setFitMethod(Settings.Fit.INSIDE)
                .setGravity(Gravity.CENTER);
        Glide.with(this.context).load(getImageAt(position)).into(imageView);

        TextView textView = view.findViewById(R.id.textView2);
        switch (position){
            case 0:
                textView.setText("Lướt sang trái tại ảnh để hiển thị người tiếp theo ");
                break;
            case 1:
                textView.setText("Lướt lên tại ảnh để xem thông tin người dùng khác");
                break;
            case 2 :
                textView.setText("Nhấn vào ảnh đại diện để chat với người bạn muốn");
                break;
            case 3:
                textView.setText("Nhấn và giữ để block một ai đó");
                Button button = view.findViewById(R.id.gotit);
                button.setVisibility(View.VISIBLE);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(context,MainMenu.class);
                        intent.putExtra("userInfo",user);
                        intent.putExtra("bio",bio);

                        context.startActivity(intent);
                        ((Activity)context).finish();
                    }
                });
                break;
        }
        ((ViewPager)container).addView(view);
        return view;
    }
    /*
    This callback is responsible for destroying a page. Since we are using view only as the
    object key we just directly remove the view from parent container
    */
    @Override
    public void destroyItem(ViewGroup container, int position, Object view) {
        View viewItem = (View) view;
        ((ViewPager) container).removeView(viewItem);
        viewItem = null;

    }
    /*
    Returns the count of the total pages
    */
    @Override
    public int getCount() {
        return imagesLinks.length;
    }
    /*
    Used to determine whether the page view is associated with object key returned by instantiateItem.
    Since here view only is the key we return view==object
    */
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return object == view;
    }
    private String getImageAt(int position) {
        return imagesLinks[position];
    }
}
