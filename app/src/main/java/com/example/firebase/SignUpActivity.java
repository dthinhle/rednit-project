package com.example.firebase;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.app.LoaderManager.LoaderCallbacks;

import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;

import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.Manifest.permission.READ_CONTACTS;



/**
 * A login screen that offers login via email/password.
 */
public class SignUpActivity extends AppCompatActivity {

    String passValue;
    boolean check;
    public DatabaseReference mDatabase;
    Aleart aleart;

    EditText name ;
    EditText pass ;
    EditText email ;
    TextView date ;
    EditText phone;

    RadioGroup RG ;
    int selectedID ;
    RadioButton gender ;
    String dateOut;

    boolean validAge;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_createaccount);
        mDatabase = FirebaseDatabase.getInstance().getReference();


         name = findViewById(R.id.username);
         pass = findViewById(R.id.password);
         email = findViewById(R.id.email);
         date = findViewById(R.id.birthdayInput);
         phone = findViewById(R.id.phone);
         RG = findViewById(R.id.RG);


        name.addTextChangedListener(ntw);
//        pass.addTextChangedListener(ptw);
        email.addTextChangedListener(etw);

        phone.addTextChangedListener(phonetw);

       RG.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
           @Override
           public void onCheckedChanged(RadioGroup group, int checkedId) {
               selectedID = RG.getCheckedRadioButtonId();
               gender = findViewById(selectedID);
               if(gender != null){
                   String genderString = gender.getText().toString();
                   genderCheck(genderString);

               }



               TextView tv = findViewById(R.id.genderError);
               tv.setText(" ");
           }
       });


        final Calendar myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener datePicker = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateEditText( myCalendar);
            }

        };

        date.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                new DatePickerDialog(SignUpActivity.this, datePicker, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });



    }

    private String getAge(String dobString){

        Date date = null;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        try {
            date = sdf.parse(dobString);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(date == null) return "";

        Calendar dob = Calendar.getInstance();
        Calendar today = Calendar.getInstance();

        dob.setTime(date);

        int year = dob.get(Calendar.YEAR);
        int month = dob.get(Calendar.MONTH);
        int day = dob.get(Calendar.DAY_OF_MONTH);

        dob.set(year, month+1, day);

        int age = today.get(Calendar.YEAR) - dob.get(Calendar.YEAR);

        if (today.get(Calendar.DAY_OF_YEAR) < dob.get(Calendar.DAY_OF_YEAR)){
            age--;
        }



        return Integer.toString(age);
    }


    private void updateEditText(Calendar myCalendar) {


        String myFormat = "dd/MM/yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.CHINA);

       dateOut=sdf.format(myCalendar.getTime());
       int age = Integer.parseInt(getAge(dateOut));
       if(age < 18){
           validAge = false;
           date.setTextColor(Color.RED);
           date.setText(getResources().getString(R.string.error_invalid_age));
       }
       else{
           validAge = true;
           date.setTextColor(Color.BLACK);
           date.setText(dateOut);

       }

    }
    private boolean isEmailValid(CharSequence email) {
        if( !android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()){
            TextView tv = findViewById(R.id.emailError);
            tv.setText(getResources().getString(R.string.error_invalid_email));
            return false;
        }
        return true;

    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic
        if(password.length() < 4){
            pass.setText("");
            pass.setHint(getResources().getString(R.string.error_invalid_password));
            pass.setHintTextColor(Color.RED);
            return false;
        }
        return true;
    }


    private boolean nameCheck(String name){
        if(name.isEmpty()){
            TextView tv = findViewById(R.id.nameError);
            tv.setText(getResources().getString(R.string.blank));
            return false;
        }
        else if(name.contains(" ")){
            TextView tv = findViewById(R.id.nameError);
            tv.setText(getResources().getString(R.string.spaceInName));
            return false;
        }
        else{
            TextView tv = findViewById(R.id.nameError);
            tv.setText(" ");

        }
        return true;
    }

    private boolean genderCheck(String gender){
        if(gender==null){
            return false;
        }
        return true;
    }


    TextWatcher ntw = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

                nameCheck(name.getText().toString());



        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };
    TextWatcher etw = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            isEmailValid(email.getText().toString());
            TextView tv = findViewById(R.id.emailError);
            tv.setText(" ");
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };
//    TextWatcher ptw = new TextWatcher() {
//        @Override
//        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//        }
//
//        @Override
//        public void onTextChanged(CharSequence s, int start, int before, int count) {
//            isPasswordValid(pass.getText().toString());
//            TextView tv = findViewById(R.id.wrongPassError);
//            tv.setText(" ");
//        }
//
//        @Override
//        public void afterTextChanged(Editable s) {
//
//        }
//    };


    TextWatcher phonetw = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

                TextView tv = findViewById(R.id.phoneError);
                try{
                    if(phone.getText().length() > 10 || phone.getText().toString().charAt(0)!= '0'|| phone.getText().length() < 8){

                        tv.setText(getResources().getString(R.string.error_invalid_phonenumber));
                    }
                    else{
                        tv.setText(" ");
                    }
                }catch (Exception e){

                }

        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    public boolean checkValidInput(String email, String passwword,String name, String gender){
        boolean emailCheck = isEmailValid(email);
        boolean passwordCheck = isPasswordValid(passwword);
        boolean nameAndGenderCheck = (nameCheck(name)&&genderCheck(gender));
        return (emailCheck && passwordCheck  && nameAndGenderCheck && validAge);
    }

    public void onGoback(View view){
        this.finish();
    }



    public void onSaveData(View view) {


        final String emailV = email.getText().toString();
        final String dateV = date.getText().toString();
        String genderValidation=null;

        try{
            genderValidation = gender.getText().toString();
        }catch (Exception e){
            TextView tv = findViewById(R.id.genderError);
            tv.setText(getResources().getString(R.string.blank));
        }

        final String nameV = name.getText().toString();
        final String genderV = genderValidation;
        String passV = pass.getText().toString();
        final String phoneNumber = phone.getText().toString();

        if(!checkValidInput(emailV,passV,nameV,genderV)){
           return;
        }


        else{
            try {
                // Create MessageDigest instance for MD5
                MessageDigest md = MessageDigest.getInstance("MD5");
                //Add password bytes to digest
                md.update(passV.getBytes());
                //Get the hash's bytes
                byte[] bytes = md.digest();
                //This bytes[] has bytes in decimal format;
                //Convert it to hexadecimal format
                StringBuilder sb = new StringBuilder();
                for(int i=0; i< bytes.length ;i++)
                {
                    sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
                }
                //Get complete hashed password in hex format
                passV = sb.toString();
                passValue = passV;
            }

            catch (NoSuchAlgorithmException e)
            {
                e.printStackTrace();
            }
            //check if username exists or not
            mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.hasChild(nameV)){
                        aleart = new Aleart(1,getString(R.string.error),SignUpActivity.this,"","");

                    }
                    else{
                        try{
                            check = true;
                            String avaU ="https://firebasestorage.googleapis.com/v0/b/rednit-project.appspot.com/o/default-avatar.png?alt=media&token=59e48875-0459-474d-a445-445025e6aeef";
                            User user = new User(nameV,nameV ,passValue,emailV,dateV,genderV,"user",avaU,phoneNumber);
                            String userId = nameV;
                            mDatabase.child(userId).setValue(user);
                            mDatabase.child(userId).child("settings").child("null").setValue("null");
                            mDatabase.child(userId).child("friends").child("null").setValue("null");
                            mDatabase.child(userId).child("chatlist").child("null").setValue("null");


                        }
                        finally {

                            aleart = new Aleart(2,getString(R.string.registerOK),SignUpActivity.this,"nameV",name.getText().toString());
                        }

                    }

                }



                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {
                    Log.w("Some thing went wrong","Error");
                }
            });






        }

    }
    @Override
    public void onBackPressed(){
        super.onBackPressed();
        finish();
    }

}

