package com.example.firebase;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.transition.Transition;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ChatHistory extends AppCompatActivity {

    DatabaseReference mDatabase;
    DatabaseReference theirDatabase;
    DatabaseReference mineblockRef;
    DatabaseReference theirblockRef;
    User user;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_history);

        Intent intent = getIntent();
        mineblockRef = FirebaseDatabase.getInstance().getReference();
        theirblockRef = FirebaseDatabase.getInstance().getReference();
        user = (User) intent.getSerializableExtra("userInfo");
        loadchatList();
    }

    public void loadchatList(){
        mDatabase = FirebaseDatabase.getInstance().getReference().child(user.name);
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild("chatbox")){
                    for(DataSnapshot ds : dataSnapshot.child("chatbox").getChildren()){
                        if(ds.hasChild("blocked")){
                            if(!ds.child("blocked").getValue().toString().equals("true")){
                                final String chatName = ds.getKey().toString();
                                theirDatabase = FirebaseDatabase.getInstance().getReference().child(chatName);
                                theirDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                        User receiver = dataSnapshot.getValue(User.class);
                                        createButton(receiver);
                                    }

                                    @Override
                                    public void onCancelled(@NonNull DatabaseError databaseError) {

                                    }
                                });
                            }
                        }
                        else{
                            final String chatName = ds.getKey().toString();

                            theirDatabase = FirebaseDatabase.getInstance().getReference().child(chatName);
                            theirDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                                    User receiver = dataSnapshot.getValue(User.class);
                                    createButton(receiver);
                                }

                                @Override
                                public void onCancelled(@NonNull DatabaseError databaseError) {

                                }
                            });
                        }






                    }
                }



            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


    public void createButton(final User receiver){
        // Create a LinearLayout element
        final LinearLayout linearLayout = findViewById(R.id.LN_layout1);
        final Button button = new Button(this);
        // Add Buttons
        button.setText(receiver.nickname);
        button.setCompoundDrawablePadding(50);
        button.setGravity(Gravity.LEFT|Gravity.CENTER);
        button.setAllCaps(false);
        Glide.with(this)
                .load(receiver.avatarUrl)
                /* Because we can*/
                .apply(RequestOptions.circleCropTransform())
                /* If a fallback is not set, null models will cause the error drawable to be displayed. If
                 * the error drawable is not set, the placeholder will be displayed.*/
                .apply(RequestOptions.placeholderOf(R.drawable.da))
                .apply(new RequestOptions().override(100, 100))

                .into(new SimpleTarget<Drawable>() {
                    @Override
                    public void onResourceReady(@NonNull Drawable resource,
                                                @Nullable Transition<? super Drawable> transition) {
                        /* Set a drawable to the left of textView */
                        button.setCompoundDrawablesWithIntrinsicBounds(resource, null, null, null);
                    }
                });
        linearLayout.addView(button);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onGotoChat(receiver);
            }
        });
        button.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                AlertDialog.Builder adb = new AlertDialog.Builder(ChatHistory.this);


                adb.setMessage("Bạn có muốn block user này không ?");
                adb.setTitle("Block user này");
                adb.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        mineblockRef.child(user.name).child("blocklist").child(receiver.name).setValue(receiver.nickname);
                        theirblockRef.child(receiver.name).child("blocklist").child(user.name).setValue(user.nickname);
                        mineblockRef.child(user.name).child("chatbox").child(receiver.name).child("blocked").setValue("true");
                        theirblockRef.child(receiver.name).child("chatbox").child(user.name).child("blocked").setValue("true");
                        linearLayout.removeView(button);
                    }
                });
                adb.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                AlertDialog ad = adb.create();
                ad.show();
                return true;
            }
        });

    }


    public void onGotoChat(User withWhom){
        Intent intent = new Intent(this,Chat.class);
        intent.putExtra("userInfo",user);
        intent.putExtra("withWhom",withWhom);
        startActivity(intent);
    }
    @Override
    public void onBackPressed(){
        super.onBackPressed();
        finish();
    }
}
