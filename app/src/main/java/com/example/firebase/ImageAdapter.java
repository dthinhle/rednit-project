package com.example.firebase;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ImageAdapter extends BaseAdapter {
    public Context context;
    public String [] images;
    public  String [] deleteimages;

    public HashMap<Integer,String> deleteMap = new HashMap<Integer, String>();



    public List<String> list = new ArrayList<String>(){

    };

    public void updateImageList(String newImage) {

        list.add(newImage);
        listToArray();
    }

    public void removeAt(int position){
        list.remove(position);
        listToArray();
    }

    public void listToArray(){
        images = new String[list.size()];
        images = list.toArray(images);


    }


    public ImageAdapter(Context c){
        context = c;
    }



    @Override
    public int getCount() {
        return images.length;
    }

    @Override
    public Object getItem(int position) {
        return images[position];
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ImageView imageView = new ImageView(context);

        Glide.with(parent).load(images[position]).into(imageView);
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        imageView.setPadding(0,20,0,50);
        imageView.setLayoutParams(new GridView.LayoutParams(300,300));


        return imageView;
    }

}
