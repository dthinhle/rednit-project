package com.example.firebase;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

public class TheirInformation extends AppCompatActivity {

    TextView theirNickname;
    TextView theirDate;
    TextView theirEmail;
    TextView theirGender;
    TextView theirPhone;
    ImageView imageView;
    TextView theirbio;

    DatabaseReference mDatabase;
    GridView gridView;
    ImageAdapter imageAdapter ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_their_information);

        Intent intent = getIntent();
        User theirInfo = (User)intent.getSerializableExtra("theirInfo");
        mDatabase = FirebaseDatabase.getInstance().getReference().child(theirInfo.name);

        theirDate = findViewById(R.id.theirdate_field);
        theirEmail = findViewById(R.id.theirEmail_feild);
        theirGender = findViewById(R.id.theirgender_field);
        theirPhone = findViewById(R.id.theirphone_field);
        theirNickname = findViewById(R.id.theirname_field);
        imageView = findViewById(R.id.theirAvatar);
        theirbio = findViewById(R.id.theirbio_field);

        imageAdapter = new ImageAdapter(this);
        gridView = findViewById(R.id.showImages);

        theirNickname.setText(theirInfo.nickname);

        showTheirInfo(theirInfo);

        Glide.with(this).load(theirInfo.avatarUrl).into(imageView);

        loadImages();


    }


    public void showTheirInfo(final User theirInfo){



        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild("hide")){

                    // hide
                    for(DataSnapshot ds : dataSnapshot.child("hide").getChildren()){

                            if(ds.getKey().toString().equals("gender")){
                                if(ds.getValue().toString().equals("true")){
                                    theirGender.setText( "Ẩn");
                                }
                            }
                            if(ds.getKey().toString().equals("phone")){
                                if(ds.getValue().toString().equals("true")){
                                    theirPhone.setText( "Ẩn");
                                }
                            }
                            if(ds.getKey().toString().equals("email")){
                                if(ds.getValue().toString().equals("true")){
                                    theirEmail.setText( "Ẩn");
                                }

                            }
                            if(ds.getKey().toString().equals("date")){
                            if(ds.getValue().toString().equals("true")){
                                theirDate.setText( "Ẩn");
                            }

                        }

                    }

                    // show which are not hided ( handle first time use)
                        if(theirGender.getText().toString().equals("")){
                            theirGender.setText(theirInfo.gender);
                        }
                        if(theirDate.getText().toString().equals("")){
                            theirDate.setText(theirInfo.date);
                        }
                        if(theirPhone.getText().toString().equals("")){
                            theirPhone.setText(theirInfo.phone);
                        }
                        if(theirEmail.getText().toString().equals("")){
                            theirEmail.setText(theirInfo.email);
                        }
                    }
                else{
                    theirGender.setText(theirInfo.gender);
                    theirEmail.setText(theirInfo.email);
                    theirPhone.setText(theirInfo.phone);
                    theirDate.setText(theirInfo.date);
                }

                if(dataSnapshot.hasChild("bio")){
                    theirbio.setText(dataSnapshot.child("bio").getValue().toString());
                }
                else{
                    theirbio.setText("Chưa có thông tin");
                }

        }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }

            });



    }


    public void loadImages(){
        mDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange( DataSnapshot dataSnapshot) {
                if(dataSnapshot.hasChild("Images")){
                    for(DataSnapshot ds : dataSnapshot.child("Images").getChildren()){
                        String Url = ds.getValue(String.class);
                        imageAdapter.list.add(Url);

                    }
                }
                imageAdapter.listToArray();
                gridView.setAdapter(imageAdapter);

            }

            @Override
            public void onCancelled( DatabaseError databaseError) {

            }
        });
    }
    @Override
    public void onBackPressed(){
        super.onBackPressed();
        finish();
    }
}