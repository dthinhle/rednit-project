#  Project: REDNIT

Project: Rednit is basically an Android mini Tinder clone. Rednit provides dating suggestions for you, a basic user info page and a image gallery.

## Where can I get it?

Download from [Gitlab Releases](https://gitlab.com/dthinhle/rednit-project/releases).

## How to use it?

### Create your account

- First of all, you need to download the apk file from Gitlab Release and installing it into your phone.

**NOTE**: Because the app isn't send to [Play Protect Appeals Submission Form](https://support.google.com/googleplay/android-developer/contact/protectappeals) so when installing to your device(s) the process is going to fail. To prevent this, before installing Rednit open your Play Store, select Play Protect from the side menu and disable it. Don't forget to re-enable Play Protect after the installation. 

- Open Rednit, then press "Tạo tài khoản" button.
- Fill all the required informations and press "Đăng ký" button.
- Voilà, your account are ready to go!

### Basic Tinder-like feature

- After log into your account. You are going to see an instruction for first-time user.
- In case you skip that instruction. Here is the short version of it:
  - Swipe right to left to skip a person you don't find interesting.
  - Swipe up to show the information of the current suggestion.
  - Tap on the profile picture to start talking with him/her.
  - Tap and hold on the profile picture to block someone.
    **NOTE**: When you block someone, they won't get you as their suggestion and neither you will, your conversation will be hidden if already exists.
  - To select a specific gender as your preferences, simply press on the combobox.
  - To access other feature, press on the menu circle.
- To log out, you can either double press back or choose "Thông tin cá nhân" in the menu circle and press "Đăng xuất" in the upcoming activity.

### Your information

To view your information, press "Thông tin cá nhân" in the menu circle.
- In this activity, you can configure what informations of these to display: Gender, Email, Phone number and your birthday.
- To update your profile picture, simply tap on your current profile picture.
- To edit your information, press the pencil icon next to your profile picture. On your upcoming activity, type in whatever information you desire to correct.

    **NOTE**: Your birthday is not editable because this app is for people over 18 year olds. So, get your birthday right the first time, you don't have any chance to correct it.

### Your conversations

To view your conversations, press "Lịch sử chat" in the menu circle.
- Your past conversations will show up here.
- To send message to someone, press on their name and your conversation with him/her will show up. Only text-based message is supported.
- To block someone from sending message to you. You can press and hold on their name. Confirm the upcoming dialog.

### Your gallery

To view your gallery, press "Thư viện ảnh" in the menu circle.
- Your uploaded image on the app will show up.
- To either add or delete image, press the menu circle and select coresponding button.
- To view a image, single press on the image. swipe left or right to view next and previous photo.
- To delete multiple images. Press and hold on every single one of your image and select Delete in the circle menu.

### Miscellaneous

- To send any feedback to the Rednit Developers, press "Góp ý" in the circle menu.
- To remove anyone from your block list (if you change your mind), press "Danh sách chặn" and press on the button next to their name.

## Library source :

- Boomenu: [by Nightone on github](https://github.com/Nightonke/BoomMenu)
- BackgroundMail: [by Abdul Rizwan on StackOverFlow](https://stackoverflow.com/questions/29918632/automatically-mail-send-in-background-in-android/29919122)
- Glide - image loader: [by bumptech on github](https://github.com/bumptech/glide)
- Circle image view: [by hdodenhof on github](https://github.com/hdodenhof/CircleImageView)
- Media picker: [by alhazmy13 on github](https://github.com/alhazmy13/MediaPicker)
- CircleIndicator: [by ongakuer on github](https://github.com/ongakuer/CircleIndicator)
- MaterialTextField: [by florent37](https://github.com/florent37/MaterialTextField)
- GestureViews: [by alexvasilkov on github](https://github.com/alexvasilkov/GestureViews)
- Firebase database: [by google firebase](https://firebase.google.com/)

